<?php

/**
 * 抽象工厂模式
 *   
 * 
 * 抽象工厂(Abstract Factory)角色：它声明一个创建抽象产品对象的接口。通常以接口或抽象类实现，所有的具体工厂类必须实现这个接口或继承这个类。
 * 具体工厂(Concrete Factory)角色：实现创建产品对象的操作。客户端直接调用这个角色创建产品的实例。这个角色包含有选择合适的产品对象的逻辑。通常使用具体类实现。
 * 抽象产品(Abstract Product)角色：声明一类产品的接口。它是工厂方法模式所创建的对象的父类，或它们共同拥有的接口。
 * 具体产品(Concrete Product)角色：实现抽象产品角色所定义的接口，定义一个将被相应的具体工厂创建的产品对象。其内部包含了应用程序的业务逻辑。
 *以下情况应当使用抽象工厂模式：
 *1、一个系统不应当依赖于产品类实例如何被创建、组合和表达的细节，这对于所有形态的工厂模式都是重要的。
 *2、这个系统的产品有多于一个的产品族，而系统只消费其中某一族的产品。
 *3、 同属于同一个产品族的产品是在一起使用的，这一约束必须在系统的设计中体现出来。
 *4、系统提供一个产品类的库，所有的产品以同样的接口出现，从而使用客户端不依赖于实现
 *
 *场景:1、QQ 换皮肤，一整套一起换。 2、生成不同操作系统的程序。
 *
 *介绍
        意图：提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类。
        
        主要解决：主要解决接口选择的问题。
        
        何时使用：系统的产品有多于一个的产品族，而系统只消费其中某一族的产品。
        
        如何解决：在一个产品族里面，定义多个产品。
        
        关键代码：在一个工厂里聚合多个同类产品。
        
        应用实例：工作了，为了参加一些聚会，肯定有两套或多套衣服吧，比如说有商务装（成套，一系列具体产品）、时尚装（成套，一系列具体产品），甚至对于一个家庭来说，可能有商务女装、商务男装、时尚女装、时尚男装，这些也都是成套的，即一系列具体产品。假设一种情况（现实中是不存在的，要不然，没法进入共产主义了，但有利于说明抽象工厂模式），在您的家中，某一个衣柜（具体工厂）只能存放某一种这样的衣服（成套，一系列具体产品），每次拿这种成套的衣服时也自然要从这个衣柜中取出了。用 OO 的思想去理解，所有的衣柜（具体工厂）都是衣柜类的（抽象工厂）某一个，而每一件成套的衣服又包括具体的上衣（某一具体产品），裤子（某一具体产品），这些具体的上衣其实也都是上衣（抽象产品），具体的裤子也都是裤子（另一个抽象产品）。
        
        优点：当一个产品族中的多个对象被设计成一起工作时，它能保证客户端始终只使用同一个产品族中的对象。
        
        缺点：产品族扩展非常困难，要增加一个系列的某一产品，既要在抽象的 Creator 里加代码，又要在具体的里面加代码。
        
        使用场景： 1、QQ 换皮肤，一整套一起换。 2、生成不同操作系统的程序。
        
        注意事项：产品族难扩展，产品等级易扩展。
 */

// 抽象工厂
interface AnimalFactory
{

    public function createCat();

    public function createDog();
}

// 具体工厂
class BlackAnimalFactory implements AnimalFactory
{

    function createCat()
    {
        return new BlackCat();
    }

    function createDog()
    {
        return new BlackDog();
    }
}

class WhiteAnimalFactory implements AnimalFactory
{

    function createCat()
    {
        return new WhiteCat();
    }

    function createDog()
    {
        return new WhiteDog();
    }
}

// 抽象产品
interface Cat
{

    function Voice();
}

interface Dog
{

    function Voice();
}

// 具体产品
class BlackCat implements Cat
{

    function Voice()
    {
        echo '黑猫喵喵……';
    }
}

class WhiteCat implements Cat
{

    function Voice()
    {
        echo '白猫喵喵……';
    }
}

class BlackDog implements Dog
{

    function Voice()
    {
        echo '黑狗汪汪……';
    }
}

class WhiteDog implements Dog
{

    function Voice()
    {
        echo '白狗汪汪……';
    }
}
// 生产者
class Client
{

    public static function main()
    {
        self::run(new BlackAnimalFactory());
        self::run(new WhiteAnimalFactory());
    }

    public static function run(AnimalFactory $AnimalFactory)
    {
        $cat = $AnimalFactory->createCat();
        $cat->Voice();
        
        $dog = $AnimalFactory->createDog();
        $dog->Voice();
    }
}
Client::main();

/**
 * 白狗 --->狗(接口)<-----黑狗
 * 叫                                                         叫
 * 白猫--->猫(接口)<-----黑猫
 * 叫                                                         叫
 *
 * 白色动物工厂--->动物工厂(接口)<--黑色动物工厂
 * 创建 狗                                                                 创建狗
 * 创建 猫                                                                  创建猫
 *
 *
 * 比如：
 * 在这种情况下，抽象工厂是创建一些组件的契约
 * 在 Web 中。 呈现文本的方式：HTML 和 JSON xml
 *
 * json--->text(接口)<---html
 * 输出                            |          输出
 *             |
 *            xml 输出
 *
 *
 * json 工厂--->text工厂(接口)<-----html工厂
 * 创建Text          |            创建Text
 *                 |
 *                  xml
 *                 创建Text
 *
 */
interface Text
{

    public function printText();
}

class Json implements Text
{

    /**
     *
     * {@inheritdoc}
     *
     * @see Text::printText()
     */
    public function printText()
    {
        // TODO Auto-generated method stub
        echo "json text";
    }
}

class Html implements Text
{

    /**
     *
     * {@inheritdoc}
     *
     * @see Text::printText()
     */
    public function printText()
    {
        // TODO Auto-generated method stub
        echo "html text";
    }
}

class Xml implements Text
{

    /**
     *
     * {@inheritdoc}
     *
     * @see Text::printText()
     */
    public function printText()
    {
        // TODO Auto-generated method stub
        echo "xml text";
    }
}

interface TextFactory
{

    public function createText(): Text;
}

class JsonFactory implements TextFactory
{

    /**
     *
     * {@inheritdoc}
     *
     * @see TextFactory::createText()
     */
    public function createText(): Text
    {
        // TODO Auto-generated method stub
        return new Json();
    }
}

class HtmlFactory implements TextFactory
{

    /**
     *
     * {@inheritdoc}
     *
     * @see TextFactory::createText()
     */
    public function createText(): Text
    {
        // TODO Auto-generated method stub
        return new Html();
    }
}

class XmlFactory implements TextFactory
{

    /**
     *
     * {@inheritdoc}
     *
     * @see TextFactory::createText()
     */
    public function createText(): Text
    {
        // TODO Auto-generated method stub
        return new Xml();
    }
}

// 生产者
class Client2
{

    public static function main()
    {
        self::run(new JsonFactory());
        self::run(new HtmlFactory());
        self::run(new XmlFactory());
    }

    public static function run(TextFactory $text)
    {
        $text->createText()->printText();
    }
}
Client2::main();
