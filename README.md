# 23种设计模式练习

#### 项目介绍
 PHP 设计模式全集 2018  https://laravel-china.org/docs/php-design-patterns/2018
23种设计模式练习+练习
####

 创建型
####
 抽象工厂模式（Abstract Factory） 已完成
####
 建造者模式（Builder） 已完成
####
 工厂方法模式（Factory Method） 已完成
####
 多例模式（Multiton） 已完成
####
 对象池模式（Pool） 已完成
####
 原型模式（Prototype） 已完成
####
 简单工厂模式（Simple Factory） 已完成
####
 单例模式（Singleton） 已完成
####
 静态工厂模式（Static Factory） 已完成
####
 结构型
####
 适配器模式（Adapter） 已完成
####
 桥梁模式（Bridge） 已完成
####
 组合模式（Composite） 已完成
####
 数据映射模式（Data Mapper） 已完成
####
 装饰模式（Decorator） 已完成
####
 依赖注入模式（Dependency Injection） 已完成
####
 门面模式（Facade） 已完成
####
 流接口模式（Fluent Interface） 已完成
####
 享元模式（Flyweight） 已完成
####
 代理模式（Proxy） 已完成
####
 注册模式（Registry） 已完成
####
 行为型
####
 责任链模式（Chain Of Responsibilities） 已完成
####
 命令行模式（Command） 已完成
####
 迭代器模式（Iterator） 已完成
####
 中介者模式（Mediator） 已完成
####
 备忘录模式（Memento） 已完成
####
 空对象模式（Null Object） 已完成
####
 观察者模式（Observer） 已完成
####
 规格模式（Specification） 已完成
####
 状态模式（State） 已完成
####
 策略模式（Strategy） 已完成
####
 模板方法模式（Template Method） 已完成
####
 访问者模式（Visitor） 已完成
####
 更多类型
####
 委托模式（Delegation） 已完成
####
 服务定位器模式（Service Locator） 已完成
####
 资源库模式（Repository） 已完成
####
 实体属性值模式（EAV 模式） 已完成
####
 附录
####
 反面模式（Anti-pattern）
####
